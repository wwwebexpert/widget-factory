(function() {    
  // Localize jQuery variable
  var jQuery;
  /******** Load jQuery if not present *********/
  if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.12.4') {
      var script_tag = document.createElement('script');
      script_tag.setAttribute("type","text/javascript");
      script_tag.setAttribute("src",
          "http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js");
      if (script_tag.readyState) {
        script_tag.onreadystatechange = function () { // For old versions of IE
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                scriptLoadHandler();
            }
        };
      } else {
        script_tag.onload = scriptLoadHandler;
      }
      // Try to find the head, otherwise default to the documentElement
      (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
  } else {
      // The jQuery version on the window is the one we want to use
      jQuery = window.jQuery;
      main();
  }

  /******** Called once jQuery has loaded ******/
  function scriptLoadHandler() {
      // Restore $ and window.jQuery to their previous values and store the
      // new jQuery in our local jQuery variable
      jQuery = window.jQuery.noConflict(true);
      // Call our main function
      main(); 
  }

  function loadCssFiles($){
      var base_path     = "https://wajooba.xyz/release/build/"
      // var base_path = "build/";
      var sysbundle_css_file = base_path+"styles/css/"+"sysbundle-4ec8aeb4.css"
      var appbundle_css_file = base_path+"styles/css/"+"appbundle-7cbf75d5.css"
      var schedule_css_file  = base_path+"styles/css/"+"schedule-a-0753c508.css" 
      var cleanslate_css_file = "https://cdnjs.cloudflare.com/ajax/libs/cleanslate/0.10.1/cleanslate.min.css"

      var cssArray = [
          sysbundle_css_file,
          appbundle_css_file,
          schedule_css_file,
          cleanslate_css_file
      ]
      console.log (cssArray)

      $.each(cssArray,function(idx,value){            
          var css_file = $("<link>", { 
              rel: "stylesheet", 
              type: "text/css", 
              href: value
          });
          $(css_file).appendTo('head');
          console.log("loaded "+value)
      })  
  };

  /*
  <script src="https://wajooba.xyz/release/build/vendor-0cb86258.js"></script>
			<script src="https://wajooba.xyz/release/build/app-72f28d87.js"></script>
			<script src="https://wajooba.xyz/release/build/vendor.ui-ce9b4158.js"></script>
			<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
		*/
  function loadJsFiles($){
      var base_path     = "https://wajooba.xyz/release/build/"        
      // var base_path = "build/";
      var app_js_file = base_path+"app-72f28d87.js"
      var vendor_js_file = base_path+"vendor-0cb86258.js"
      var vendor_ui_js_file = base_path+"vendor.ui-ce9b4158.js"
      var ckeditor_js_file = "https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"
      var jsArray =[ vendor_js_file,
                    app_js_file,
                    vendor_ui_js_file,
                    ckeditor_js_file ]
      $.each(jsArray,function(idx,value){
          console.log("attempt to load js:"+ value)
          var script_tag = document.createElement('script');
          script_tag.setAttribute("type","text/javascript");
          script_tag.setAttribute("src",value);
          $(script_tag).appendTo('head');
          console.log("loaded "+value)
      })           
  }

  function appendAppBody($,location){
    var app = '<div data-smart-device-detect'+
              'data-smart-fast-click'+
              'data-smart-layout'+
              'data-smart-page-title="Wajooba" class="smart-style-0">'+			
              '<div id="orgid" style="display:none">'+ location+'</div>'+
              '<div data-ui-view="root"  data-autoscroll="false" ng-controller="byRootController"></div>'+
              '</div>'
    $('#example-widget-container').html(app);     
    $('#example-widget-container').show();
  }
  /******** Our main function ********/
  function main() { 
      jQuery(document).ready(function($) { 
          /******* Load CSS *******/
          console.log ("started loading")
          loadCssFiles($)  
          /******* Load HTML *******/
          var location=$("#orgid").text()
          console.log("orgid is "+ location)
          appendAppBody($,location)
          loadJsFiles($)              
      });
  }
})(); // We call our anonymous function immediately
